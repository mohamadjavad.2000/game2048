import java.util.*;
import java.io.*;

import pleyer.Player;

public class Game2048 {
	public static void main(String[] args) throws IOException, InterruptedException {
		Scanner s = new Scanner(System.in);
		Player player1 = new Player();
		System.out.print("Please enter your name : ");
		String name = s.nextLine();
		player1.setName(name);
		int size;
		/** get size from user */
		while (true) {
			System.out.print("\nPlease, enter size of table [2-15] : ");
			if (s.hasNextInt()) {
				size = s.nextInt();
				if (size > 1 && size < 16)
					break;
			} else
				s.next();
			System.out.println("invlaid size!");
		}
		player1.setSize(size);
		player1.startGame();
		char chKey; /** for moving {a,w,d,s} */
		String str;/** to get inputs */
		printTable(player1, size);
		System.out.print(
				"start your moves with {a,w,d,s} then press enter...\n for previouse move press p !\nYour move :");
		while (s.hasNext()) {
			str = s.next();
			printNewPage();
			if (str.length() == 1) {
				chKey = str.charAt(0);
				switch (chKey) {
				case 'w':
					if (player1.isLeftLegal()) {
						player1.up();
						/** check if it was illegal but we didn't know */
						if (player1.wasLegal()) {
							player1.updatePrev();
							break;
						}
					}
					System.out.println("ILLEGAL : you can't move up !");
					break;
				case 'd':
					if (player1.isRightLegal()) {
						player1.right();
						/** check if it was illegal but we didn't know */
						if (player1.wasLegal()) {
							player1.updatePrev();
							break;
						}
					}
					System.out.println("ILLEGAL : you can't move to the right !");
					break;
				case 'a':
					if (player1.isLeftLegal()) {
						player1.left();
						/** check if it was illegal but we didn't know */
						if (player1.wasLegal()) {
							player1.updatePrev();
							break;
						}
					}
					System.out.println("ILLEGAL : you can't move to the left !");
					break;
				case 's':
					if (player1.isDownLegal()) {
						player1.down();
						/** check if it was illegal but we didn't know */
						if (player1.wasLegal()) {
							player1.updatePrev();
							break;
						}
					}
					System.out.println("ILLEGAL : you can't move down !");
					break;
				case 'p':
					player1.prevMove();
					System.out.println("previous !");
					break;
				}
			}
			/** print table */
			System.out.println("your name : " + name + "     \t\t  your score : " + player1.getScore());
			printTable(player1, size);
			/** check if player failed */
			if (player1.failureCheck())
				break;
			System.out.print("move {a,w,d,s} & previouse move {p} \n\t\t\tYour move :");
		}
		System.out.println();
		/** end of game **/
		System.out.println("\n\n\t\t\tgame ended  \t\tyour score :" + player1.getScore());
		s.close();
	}

	private static void printTable(Player player1, int size) {
		System.out.print(" ");
		for (int row = 0; row < size; row++) {
			System.out.print("_______");
		}
		for (int row = 0; row < size; row++) {
			System.out.print("\n|");
			for (int column = 0; column < size; column++) {
				System.out.printf(" %-6d", player1.getUpdatedTable(row, column));

			}
			System.out.print("|");
		}
		System.out.print("\n ");

		for (int row = 0; row < size; row++) {
			System.out.print("_______");
		}
		System.out.println();
	}

	private static void printNewPage() throws IOException, InterruptedException {
		new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
	}
}
