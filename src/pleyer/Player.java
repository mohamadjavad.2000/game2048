package pleyer;

public class Player {
	private String name;
	private int prevScore = 0;
	private int prevScoreTemp = 0;
	private int size;
	public Table table;
	private boolean wasLegal = true;

	public boolean wasLegal() {
		return wasLegal;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void startGame() {
		table = new Table(size);
		addARandNum();
	}

	public boolean failureCheck() {
		if (table.isFull())
			return table.failureCheck();
		else
			return false;
	}

	public int getScore() {
		return table.getScore();
	}

	public boolean isUpLegal() {
		return !(table.getLastMove() == 'U' && !wasLegal);
	}

	public void up() {
		/** up-sum every column */
		for (int column = 0; column < size; column++)
			table.sumUp(column);
		manager();
	}

	public boolean isDownLegal() {
		return !(table.getLastMove() == 'D' && !wasLegal);
	}

	public void down() {
		/** down-sum every column */
		for (int column = 0; column < size; column++)
			table.sumDown(column);
		manager();
	}

	public boolean isRightLegal() {
		return !(table.getLastMove() == 'R' && !wasLegal);
	}

	public void right() {
		/** right-sum every row */
		for (int row = 0; row < size; row++)
			table.sumRight(row);
		manager();
	}

	public boolean isLeftLegal() {
		return !(table.getLastMove() == 'L' && !wasLegal);
	}

	public void left() {
		/** left-sum every row */
		for (int row = 0; row < size; row++)
			table.sumLeft(row);
		manager();
	}

	/** check if this move is legal ;if (legal) adding new random number */
	private void manager() {
		if (!table.nothingChanged()) {
			addARandNum();
			wasLegal = true;
		} else {
			wasLegal = false;
		}
	}

// public Integer[][] getUpdatedTable() {
// return table.getTable();
// }
	/** making new random 2 or 4 by random Maker */
	private void addARandNum() {
		table.randMaker();
	}

	public Integer getUpdatedTable(int row, int column) {
		return table.getTable(row, column);
	}

	public void prevMove() {
		wasLegal = true;
		table.backToPrevTable();
		table.setScore(prevScore);
	}

	public void updatePrev() {
			table.updatePrevTable();
			table.updateLastTable();
			prevScore = prevScoreTemp;
			prevScoreTemp = table.getScore();
	}
}
//System.out.println(table.table);
//System.out.println(table.lastTable);
//System.out.println(Arrays.deepToString(table.table)+"table");
//System.out.println(Arrays.deepToString(table.lastTable));