package pleyer;

import java.util.ArrayList;

public class Table {
	private Integer table[][];
	private Integer lastTable[][];
	private boolean isFull;
	private int size;
	private int score = 0;
	private char lastMove;
	private Integer[][] prevTable;

	public void setScore(int score) {
		this.score = score;
	}

	public boolean isFull() {
		return isFull;
	}

	public char getLastMove() {
		return lastMove;
	}

	public int getScore() {
		return score;
	}

	public boolean nothingChanged() {
		boolean b=true;
		for (int j = 0; j < size; j++) {
			for (int k = 0; k < size; k++) {
				if (!table[j][k].equals(lastTable[j][k])) {
					b=false;
				}
			}
		}
		return b;
	}

	Table(int size) {
		this.size = size;
		isFull = false;
		table = new Integer[size][size];
		lastTable = new Integer[size][size];
		prevTable= new Integer[size][size];
		init();
		// System.out.println(Arrays.deepToString(table));
	}

	/** initialize table indexes */
	private void init() {
		for (int j = 0; j < size; j++) {
			for (int k = 0; k < size; k++) {
				table[j][k] = 0;
				lastTable[j][k] = 0;
				prevTable[j][k] = 0;
			}

		}
		randMaker();
	}

	public void backToPrevTable() {
		tableCopier(table, prevTable);
	}
//	public void backToLastTable() {
//		tableCopier(lastTable,  table);
//	}

	/** prevTable= lastTable */
	public void updatePrevTable() {
		tableCopier(prevTable, lastTable);
	}

	/** lastTable= table */
	public void updateLastTable() {
		tableCopier(lastTable, table);
	}

	public void tableCopier(Integer[][] copy, Integer[][] orginal) {
		for (int j = 0; j < size; j++) {
			for (int k = 0; k < size; k++) {
				copy[j][k] = orginal[j][k];
			}
		}
	}

	private ArrayList<Integer> findEmptyElements() {
		ArrayList<Integer> empties = new ArrayList<Integer>();
		int row, column;
		isFull = true;
		for (row = 0; row < size; row++) {
			for (column = 0; column < size; column++) {
				if (table[row][column].equals(0)) {
					if (isFull)
						isFull = false;
					empties.add(row);
					empties.add(column);
				}
			}
		}
		// System.out.println("\nempties size: " + empties.size());
		return empties;
	}

	/**
	 * if it has empty element (=0) @return added=true & add a random number/s in an
	 * empty element of table else added=false
	 */
	public void randMaker() {
		int numOfNewRands;
		if (size < 8)
			numOfNewRands = 1;
		else
			numOfNewRands = 2;
		for (int randCounter = 0; randCounter < numOfNewRands; randCounter++) {
			int row, column;
			ArrayList<Integer> empties = findEmptyElements();
//			if (empties.size()==0)
			if (isFull) {
				break;
			}
			/** find a random empty element */
			row = (int) (Math.random() * (empties.size() - 1)) / 2;
			/**
			 * every empty-element column-index-value is stored in empties after
			 * row-index-value
			 */
			column = empties.get(row * 2 + 1);
			row = empties.get(row * 2);
			/** make a random number between 0 and 10 */
			int rand = (int) Math.random() * 10;
			/** 70 percent 2 & 30 percent 4 */
			if (rand > 7) {
				table[row][column] = 4;
			}

			else {
				table[row][column] = 2;
			}
		}
	}

	public boolean failureCheck() {
		int mergeFinder, lastMerge;
		for (int column = 0; column < size; column++) {
			lastMerge = 0;
			for (mergeFinder = 1; mergeFinder < size; mergeFinder++) {
				if (table[lastMerge][column].equals(0))
					lastMerge = mergeFinder;
				else {
					if (table[lastMerge][column].equals(table[mergeFinder][column])) {
						return false;
					} else if (!table[mergeFinder][column].equals(0)) {
						lastMerge = mergeFinder;
					}
				}
			}
		}

		for (int row = 0; row < size; row++) {
			lastMerge = 0;
			for (mergeFinder = 1; mergeFinder < size; mergeFinder++) {
				if (table[row][lastMerge].equals(0))
					lastMerge = mergeFinder;
				else {
					if (table[row][lastMerge].equals(table[row][mergeFinder])) {
						return false;
					} else if (!table[row][mergeFinder].equals(0)) {
						lastMerge = mergeFinder;
					}
				}
			}
		}
		return true;
	}

	/**
	 * sum and merge same numbers up to down & move all numbers from bottom to top
	 */
	void sumUp(int constColumn) {
		mergeUp(constColumn);
		moveUp(constColumn);
		lastMove = 'U';
	}

	/**
	 * sum and merge same numbers up to down
	 */
	void mergeUp(int constColumn) {
		int mergeFinder, lastMerge = 0;
		for (mergeFinder = 1; mergeFinder < size; mergeFinder++) {
			if (table[lastMerge][constColumn].equals(0))
				lastMerge = mergeFinder;
			else {
				if (table[lastMerge][constColumn].equals(table[mergeFinder][constColumn])) {
					table[lastMerge][constColumn] = 2 * table[lastMerge][constColumn];
					score = getScore() + table[lastMerge][constColumn];
					table[mergeFinder][constColumn] = 0;
					lastMerge = mergeFinder;
				} else if (!table[mergeFinder][constColumn].equals(0)) {
					lastMerge = mergeFinder;
				}
			}
		}
	}

	/**
	 * move all numbers from bottom to top
	 */
	public void moveUp(int constColumn) {
		int naturalNums = 0;
		int copy[] = new int[size];
		/** copying */
		for (int i = 0; i < size; i++) {
			copy[i] = table[i][constColumn];
		}
		/** find natural numbs and paste them top of the table */
		for (int j = 0; j < size; j++) {
			if (copy[j] != 0) {
				table[naturalNums][constColumn] = copy[j];
				naturalNums++;
			}
		}
		/** make remainder indexes zero (bottom indexes) */
		for (int k = naturalNums; k < size; k++) {
			table[k][constColumn] = 0;
		}
	}

	/**
	 * sum and merge same numbers down to up & move all numbers from top to bottom
	 */
	public void sumDown(int constColumn) {
		mergeDown(constColumn);
		moveDown(constColumn);
		lastMove = 'D';
	}

	/**
	 * sum and merge same numbers down to up
	 */
	private void mergeDown(int constColumn) {
		int mergeFinder, lastMerge = size - 1;
		for (mergeFinder = size - 2; mergeFinder >= 0; mergeFinder--) {
			if (table[lastMerge][constColumn].equals(0))
				lastMerge = mergeFinder;
			else {
				if (table[lastMerge][constColumn].equals(table[mergeFinder][constColumn])) {
					table[lastMerge][constColumn] = 2 * table[lastMerge][constColumn];
					score = getScore() + table[lastMerge][constColumn];
					table[mergeFinder][constColumn] = 0;
					lastMerge = mergeFinder;
				} else if (!table[mergeFinder][constColumn].equals(0)) {
					lastMerge = mergeFinder;
				}
			}
		}
	}

	/**
	 * move all numbers from top to bottom
	 */
	private void moveDown(int constColumn) {
		int naturalNums = size - 1;
		int copy[] = new int[size];
		/** copying */
		for (int i = 0; i < size; i++) {
			copy[i] = table[i][constColumn];
		}
		/** find natural numbs and paste them bottom of the table */
		for (int j = size - 1; j >= 0; j--) {
			if (copy[j] != 0) {
				table[naturalNums][constColumn] = copy[j];
				naturalNums--;
			}
		}
		/** make remainder indexes zero (top indexes) */
		for (int k = naturalNums; k >= 0; k--) {
			table[k][constColumn] = 0;
		}

	}

	/**
	 * sum and merge same numbers right to left & move all numbers from left to
	 * Right
	 */
	public void sumRight(int constRow) {
		mergeRight(constRow);
		moveRight(constRow);
		lastMove = 'R';
	}

	/**
	 * sum and merge same numbers right to left
	 */
	private void mergeRight(int constRow) {
		int mergeFinder, lastMerge = 0;
		for (mergeFinder = 1; mergeFinder < size; mergeFinder++) {
			if (table[constRow][lastMerge].equals(0))
				lastMerge = mergeFinder;
			else {
				if (table[constRow][lastMerge].equals(table[constRow][mergeFinder])) {
					table[constRow][lastMerge] = 2 * table[constRow][lastMerge];
					score = getScore() + table[constRow][lastMerge];
					table[constRow][mergeFinder] = 0;
					lastMerge = mergeFinder;
				} else if (!table[constRow][mergeFinder].equals(0)) {
					lastMerge = mergeFinder;
				}
			}
		}
	}

	/**
	 * move all numbers from left to Right
	 */
	private void moveRight(int constRow) {
		int naturalNums = size - 1;
		int copy[] = new int[size];
		/** copying */
		for (int i = 0; i < size; i++) {
			copy[i] = table[constRow][i];
		}
		/** find natural numbs and paste them Right of the table */
		for (int j = size - 1; j >= 0; j--) {
			if (copy[j] != 0) {
				table[constRow][naturalNums] = copy[j];
				naturalNums--;
			}
		}
		/** make remainder indexes zero (left indexes) */
		for (int k = naturalNums; k >= 0; k--) {
			table[constRow][k] = 0;
		}
	}

	/**
	 * sum and merge same numbers left to right & move all numbers from right to
	 * left
	 */
	public void sumLeft(int constRow) {
		mergeLeft(constRow);
		moveLeft(constRow);
		lastMove = 'L';
	}

	/**
	 * sum and merge same numbers left to right
	 */
	private void mergeLeft(int constRow) {
		int mergeFinder, lastMerge = size - 1;
		for (mergeFinder = size - 2; mergeFinder >= 0; mergeFinder--) {
			if (table[constRow][lastMerge].equals(0))
				lastMerge = mergeFinder;
			else {
				if (table[constRow][lastMerge].equals(table[constRow][mergeFinder])) {
					table[constRow][lastMerge] = 2 * table[constRow][lastMerge];
					score = getScore() + table[constRow][lastMerge];
					table[constRow][mergeFinder] = 0;
					lastMerge = mergeFinder;
				} else if (!table[constRow][mergeFinder].equals(0)) {
					lastMerge = mergeFinder;
				}
			}
		}

	}

	/**
	 * move all numbers from right to left
	 */
	private void moveLeft(int constRow) {
		int naturalNumsFiller = 0;
		int copy[] = new int[size];
		/** copying */
		for (int i = 0; i < size; i++) {
			copy[i] = table[constRow][i];
		}
		/** find natural numbs and paste them left of the table */
		for (int i = 0; i < size; i++) {
			if (copy[i] != 0) {
				table[constRow][naturalNumsFiller] = copy[i];
				naturalNumsFiller++;
			}
		}
		/** make remainder indexes zero (right indexes) */
		for (int k = naturalNumsFiller; k < size; k++) {
			table[constRow][k] = 0;
		}
	}

	public Integer getTable(int row, int column) {
		return table[row][column];
	}

}
